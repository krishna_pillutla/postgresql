/*-------------------------------------------------------------------------
 *
 * pg_partition_k.h
 *	  definition of the system "relation" relation (pg_class)
 *	  along with the relation's initial contents.
 *
 
 *
 * NOTES
 *	  the genbki.pl script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */
#ifndef PG_PARTITION_H
#define PG_PARTITION_H

#include "catalog/genbki.h"

/* ----------------
 *		
 * ----------------
 */

#define PG_PARTITION_ 9999

CATALOG(pg_partition_k,9999)
{
	Oid ParentTable; /* Parent table */
	Oid ChildTable; /*child table */
	NameData ChildTableName; 
	int32 Partitioning_Attr; /* attribute number of partitioning attribute */

	int32 Upper_bound;	/*upper bound of partition */

	
} FormData_pg_partition;



#define Natts_pg_partition					5
#define Anum_pg_partition_parentOid			1
#define Anum_pg_partition_childOid		2
#define Anum_pg_partition_childName		3
#define Anum_pg_partition_PartAttr			4
#define Anum_pg_partition_Ubound			5

#endif   /* PG_PARTITION_H */
